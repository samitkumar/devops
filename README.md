### jenkins

```sh
1. download jenkins.war file from https://jenkins.io/download/ 
or download through wget - wget http://mirrors.jenkins-ci.org/war/latest/jenkins.war 
2. open the command prompt (make sure you have java installed in the system)
3. java -jar jenkins.war  - this will start jenkins, but before ensure to add the JENKINS_HOME in your class path so that jenkins will store the configuration releated xml on that folder
4. java -jar jenkins.war --httpPort=<portNumber> will run the jenkind with mention <portNumber>
```

### Jenkins cli
```
java -jar jenkins-cli.jar help [COMMAND] [--username VAL] [--password VAL] [--password-file VAL]
Lists all the available commands or a detailed description of single command.
 COMMAND             : Name of the command
 --username VAL      : User name to authenticate yourself to Jenkins
 --password VAL      : Password for authentication. Note that passing a
                       password in arguments is insecure.
 --password-file VAL : File that contains the password
```
> or 

```sh
> for more help
http://lifuzu.com/blog/2014/04/24/playing-with-jenkins-create-job/ 

Jenkins cli is a command line tool to manage jenkins administrator

> java -jar jenkins-cli.jar -s http://localhost:8081/ -auth username:passwd/tokenn help

> create job
java -jar jenkins-cli.jar -s http://127.0.0.1:8081 -auth admin:admin create-job new_job_from_cli_3 < config.xml

> reload a particular job
java -jar jenkins-cli.jar -s http://localhost:8081/ -auth admin:admin reload-job <<job_name>> --username admin --password admin

> reload the configuration file without restart jenkins
java -jar jenkins-cli.jar -s http://localhost:8081/ -auth admin:admin reload-configuration --username admin --password admin

> remove a job
java -jar jenkins-cli.jar -s http://localhost:8081/ -auth admin:admin delete-job test_1 --username admin --password admin
```
> define a job for jenkins without UI (change will be on Disk)

```sh
in JENKIND_HOME directory follow the below structure
1. create a folder with same name based on your expected name
2. inside the jobs folder create a sub-folder with jobname 
3. place the config.xml file with proper xml tag define in jenkins.jobs.xsd which used to be start with <project>...</project>
```

### maven

```
please follow more on  - adb/adb-ui/pom.xml

* exclude a directory from webapp
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-war-plugin</artifactId>
   <version>3.1.0</version>
   <configuration>					
       <failOnMissingWebXml>false</failOnMissingWebXml>
       <!-- EXCLUDE FILE/FOLDER -->
       <warSourceExcludes>
	   **/static/
       </warSourceExcludes>					
       <webResources>
	   <!-- THE ui FOLDER AVAILABLE INSITE TARGET DIRECTORY ARE MAKING AVAILABLE IN WAR -->															
	   <webResource>
		<directory>${project.build.directory}/UI</directory>
		<filtering>true</filtering>
	   </webResource>						 
      </webResources>					 
  </configuration>
</plugin> 
```
* command to generate maven project
```
mvn -B archetype:generate \
    -DarchetypeGroupId=org.apache.maven.archetypes \
    -DgroupId=package.name \
    -DartifactId=project-name
```
* <dependencyManagement> and <pluginsManagement> are used in parem pom based pom to make that dependency available in child if child added that with less description

### Maven - Generate a maven based project from command line
````
mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=<<package.details>> -DartifactId=<<project_or_repo_name>>
````

### maven setting.xml
````
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">

  <servers>
    <server>
      <id>nexus-snapshots</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
    <server>
      <id>nexus-releases</id>
      <username>admin</username>
      <password>admin123</password>
    </server>
  </servers>

  <mirrors>
    <mirror>
      <id>central</id>
      <name>central</name>
      <url>http://192.168.33.10:10001/repository/maven-public/</url>
      <mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>

</settings>
````

### httpd configuration

```
#test loadbalancer configuration
<Proxy "balancer://mycluster">
    BalancerMember "http://localhost:3001"
    BalancerMember "http://localhost:3002"
</Proxy>
ProxyPass "/test" "balancer://mycluster"
ProxyPassReverse "/test" "balancer://mycluster"

#jenkins
<Proxy "balancer://jenkinscluster">
    BalancerMember "http://localhost:8081/jenkins"
</Proxy>
ProxyPass "/jenkins" "balancer://jenkinscluster"
ProxyPassReverse "/jenkins" "balancer://jenkinscluster"

<Proxy "balancer://expresscluster">
    BalancerMember "http://localhost:4000/mdo"
</Proxy>
ProxyPass "/import" "balancer://expresscluster"
ProxyPassReverse "/import" "balancer://expresscluster"
#nexus
ProxyRequests Off
ProxyPreserveHost On
<VirtualHost *:80>
  ServerName localhost
  ServerAdmin admin@example.com
  ProxyPass /nexus http://localhost:8082/nexus
  ProxyPassReverse /nexus http://localhost:8082/nexus
  ErrorLog logs/error_nexus.log
  CustomLog logs/access_nexus.log common
</VirtualHost>

* httpd Alias 

<IfModule alias_module>
   Alias /web/MDO /home/spa349/workspace/ui-mdo/MDO
</IfModule>


<Directory "/home/spa349/workspace/ui-mdo/MDO">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>


```

### weblogic
```
* WLST
* http://www.oracle.com/webfolder/technetwork/tutorials/obe/fmw/wls/12c/06-WLST--4474/cmdline.htm
* https://docs.oracle.com/cd/E13222_01/wls/docs90/config_scripting/using_WLST.html#1078952
```
### WLST deployment
```
shell script file(shell_script_file.sh)
============================
#!/bin/bash
echo "preReq set up..."
export JAVA_HOME=/home/spa349/Tools/jdk1.6.0_21
export MW_HOME=/home/spa349/Tools/wls1036_dev
. $MW_HOME/wlserver/server/bin/setWLSEnv.sh

echo "Invoking wlst..."
java -Xmx1g weblogic.WLST -skipWLSModuleScanning $*



python file(python_file.py)
================
#!/bin/python
import os

print('Deployment start')
connect('weblogic','weblogic@1','t3://localhost:7001')
deploy('gms','/home/spa349/workspace/gms/gms_app.war',target='AdminServer')
startApplication('gms')
stopApplication('gms')
undeploy('gms')



command to execute
===================
chmod u+x shell_script_file.sh python_file.py
./shell_script_file.sh ./python_file.py
```


### start & stop script
```
start script
-------------
export  JAVA_HOME=/home/ubuntu/software/jdk1.8.0_101;
export  JENKINS_HOME=/home/ubuntu/workspace/jenkins_home;
nohup  $JAVA_HOME/bin/java -jar /home/ubuntu/software/jenkins.war --httpPort=8081 & echo $! > jenkins_pid.txt

stop script
-------------
kill -9 $(cat jenkins_pid.txt);
rm -rf nohup.out;
rm -rf jenkins_pid.txt;
```

