### vagrant ###

* for more information
```
https://www.vagrantup.com/
```

* find a vagrant box
```
https://vagrantcloud.com/
```

* more command on vagrant
```
vagrant help - show all the available command
vagrant ssh - ssh inside the box
vagrant halt - turn off the VM
vagrant reload - restart the vm
vagrant up - will upload all the config mention in Vagrantfile and reload that inside machine
```

*download and setup v vm box
```
vagrant init ubuntu/xenial64 
vagrant up --provider virtualbox

This command means , vagrant will create a Vagrantfile and download ubuntu/xenial64 image from the repository and also it will keep a reference in oracle virtualbox
```


* example
```
In the folder structure a sample Vagrantfile is available which does below step
1. download the configure image
2. open a private ip addess to access
3. once done, you can further proceed for provisioning the machine

vagrant provision --provision=shell

the provision config will do :
1. update the apt-get lib
2. install docker
3. install jenkins and start the service
4. install apache2 and start the service
5. etc...
``` 
